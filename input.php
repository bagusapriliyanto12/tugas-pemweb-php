<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS --> 
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Input Nilai matakuliah </title>
  </head>
  <body>
  <div class="position-absolute top-50 start-50 translate-middle ">
    <h1>Hello, masukan nilai kamu</h1>


    <form action="hasil.php" method="post">
    

    <div class="input-group"> 
      <span class="input-group-text">Nama </span> 
      <input type="text" aria-label="First name" class="form-control" name="inputan_nama">
    </div>
    <br>
    <div class="input-group"> 
      <span class="input-group-text">NIM </span> 
      <input type="text" aria-label="First name" class="form-control" name="inputan_nim">
    </div>
    <br>
    <div class="input-group">
    <span class="input-group-text">Mata kuliah</span>
      <input type="text" aria-label="Last name" class="form-control" name="inputan_matkul">
    </div>
    <br> <!-- digunakan untuk pemisah -->
    <div class="input-group">
      <span class="input-group-text">Nilai UTS</span>
      <input type="text" aria-label="First name" class="form-control" name="inputan_uts">
    </div>
    <br>
    <div class="input-group">
      <span class="input-group-text">Nilai UAS</span>
      <input type="text" aria-label="First name" class="form-control" name="inputan_uas">
    </div>
    <br>
    <div class="input-group">
      <span class="input-group-text">Nilai Tugas</span>
      <input type="text" aria-label="First name" class="form-control" name="inputan_tugas">
    </div>

    <br>

      <br>
    <div class="d-grid gap-2">
  <button class="btn btn-primary" type="submit">Submit</button>
</div>
    </form>

    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  </body>
</html>